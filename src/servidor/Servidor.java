package servidor;

/*
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.*;
import java.io.*;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

// Se agregan las librerias para conectar con la BD
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 *
 * @author gmendez
 */
public class Servidor {
    ArrayList<Conexion> conexiones; 
    ServerSocket   ss; 
    Connection conn; 
    String personas = ""; 
    
    /*
    String [][] usuarios = {{"hugo",  "123"},
                            {"paco",  "345"},
                            {"luis",  "890"},
                            {"donald","678"}};
    */
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        });
        
        
    }

    public String borrar(String nombre){
        String pt1,pt2;
        pt1 = personas.substring(0,personas.indexOf(nombre));
        pt2 = personas.substring(personas.indexOf(nombre)+4,personas.length());
        return  pt1+pt2;
    }
    
    private void start() {
        this.conexiones = new ArrayList<>();
        Socket socket; 
        Conexion cnx; 
        
        try {
            // Se realiza una sola conexion
            if (conn == null){
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/encuesta?" +
"useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&user=encuesta&password=encuesta");
            }
            
            ss = new ServerSocket(4444);
            System.out.println("Servidor iniciado, en espera de conexiones");
            
            while (true){             
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size(),this.conn); 
                conexiones.add(cnx); 
                cnx.start();        
            }            
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }  catch (SQLException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    // Broadcasting
    private void difundir(String id, String mensaje) throws IOException {
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()){
                if (!id.equals(hilo.id)){
                    hilo.enviar(mensaje);
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
    
    class Conexion extends Thread {
        DataInputStream in;
        DataOutputStream    out;
        Connection     conn;
        Socket         cnx;
        Servidor       padre;
        int            numCnx = -1;
        String         id = "";
        
        public final int SIN_USER   = 0; 
        public final int USER_IDENT = 1;
        public final int PASS_PDTE  = 2; 
        public final int PASS_OK    = 3; 
        public final int CHAT       = 4; 
                
        public Conexion(Servidor padre, Socket socket, int num, Connection _conn){
            this.conn = _conn;
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+"-"+num;
        }
        
        @Override
        public void run() {
            String linea="", user="", pass="", mensaje="";
            String passValido = "";
            int estado = SIN_USER;
            int usr = -1;
            int intentos = 3;
            
            Statement stmt;
            ResultSet rset;

            String    sQuery = "";
            
            try {
                in = new DataInputStream(cnx.getInputStream());
                out = new DataOutputStream(cnx.getOutputStream());
                
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());
                                
                while(!mensaje.toLowerCase().equals("salir") && intentos>0){                   
                    switch (estado){
                        case SIN_USER:
                            out.writeUTF("Bienvenido, proporcione su usuario");
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            user = in.readUTF();
                            boolean found = false;  
                            passValido = "";
                            sQuery = "SELECT usuario,password FROM usuarios WHERE usuario = '"+user.trim()+"'";
                            
                            try {
                                stmt = conn.createStatement();
                                rset = stmt.executeQuery(sQuery);
                                
                                if (rset.next()){
                                    found = true;
                                    passValido = rset.getString("password");
                                }
                                
                            } catch (SQLException sqle){
                                
                            }
                            
                            if (!found){
                                estado = SIN_USER;
                            } else {
                                estado = PASS_PDTE;
                            }                                                                                                             
                            break;
                        case PASS_PDTE:
                            out.writeUTF("Escriba el password");
                            pass = in.readUTF();                   
                            if (pass.equals(passValido)){
                                estado = PASS_OK;
                                personas = personas + user;
                            }
                            --intentos;
                            break;
                        case PASS_OK:
                            out.writeUTF("Autenticado!");
                            out.writeUTF(personas);
                            difundir(id,user+"Entro!.");
                             difundir(id,user);
                            estado = CHAT;
                            break;
                        case CHAT:
                            mensaje = in.readUTF();
                            System.out.printf("%s - %s\n",
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ":" +mensaje);
                            
                            this.padre.difundir(this.id, this.id+" | "+user+" : "+mensaje);
                            break;
                    }                        
                }            
                this.padre.difundir(id, user);
                borrar(user);
                this.cnx.close();
                System.out.println("Cerrando conexion: "+this.id);
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }

        private void enviar(String mensaje) throws IOException {
            this.out.writeUTF(mensaje); //To change body of generated methods, choose Tools | Templates.
        } 
    } 
} 